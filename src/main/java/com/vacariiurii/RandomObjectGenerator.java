package com.vacariiurii;

import java.util.Map;

public interface RandomObjectGenerator<K, V> {
    K next() throws Exception;
    void add(Map.Entry<K, V> pair);
}
