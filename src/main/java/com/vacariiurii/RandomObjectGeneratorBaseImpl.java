package com.vacariiurii;

import java.util.*;

/**
 * RandomObjectGeneratorBaseImpl class is implementation of RandomObjectGenerator interface.
 * It's intended to generate objects from the transmitted map, where key is the object,
 * and the value is the probability of its generation.
 */
public class RandomObjectGeneratorBaseImpl implements RandomObjectGenerator <Object, Integer> {
    /** Contains the transmitted map and entries to RandomObjectGeneratorBaseImpl object. */
    private Map<Object, Integer> rootHolder;

    /** The sum of the probability of generating the current object and objects coming in front of it,
     * it's passed as a value in rootHolder. */
    private int totalRootHolderWeight = 0;

    /** A constructor that takes a LinkedHashMap as an argument, where key is an object
     * and value is the probability of its generation. In case of map is not initialized, or it is empty
     * it is thrown IllegalArgumentException.
     * @param map should be initialized as LinkedHashMap to guarantee the order of given objects.
     * @throws RandomGeneratorReturnException is thrown of internal methods for processing above. */
    public RandomObjectGeneratorBaseImpl(LinkedHashMap<Object, Integer> map) throws RandomGeneratorReturnException {
        if (map == null || map.isEmpty()) {
            throw new IllegalArgumentException("Given map refers to null or it is empty, please check!");
        }
        valueCheck(map);
        rootHolder = map;
        countTotalRootHolderWeight(rootHolder);
    }

    /** Default constructor that initializes empty rootHolder. */
    public RandomObjectGeneratorBaseImpl() {
        rootHolder = new LinkedHashMap<>();
    }

    /** Generates a random number between 0 and totalRootHolderWeight value and returns the first object,
     * the generation probability of which will be equal or greater than it.
     * @return Random object based on the probability of its generation.
     * @throws RandomGeneratorReturnException is thrown if no object was generated. */
    @Override
    public Object next() throws RandomGeneratorReturnException {
        Random r = new Random();
        int currentWeight = r.nextInt(totalRootHolderWeight);

        for (Map.Entry<Object, Integer> entry : rootHolder.entrySet()) {
            if (entry.getValue() >= currentWeight) {
                return entry.getKey();
            }
        }

        throw new RandomGeneratorReturnException("Internal exception while trying to get next object.");
    }

    /** Adds new entry to rootHolder.
     * @param pair is the new pair of key-value. */
    @Override
    public void add(Map.Entry<Object, Integer> pair)  {
        totalRootHolderWeight += pair.getValue();
        rootHolder.put(pair.getKey(), totalRootHolderWeight);
    }

    /** Calculates the total sum of all the probabilities of generating each object from map.
     * @param map is passed to this method from the constructor.*/
    private void countTotalRootHolderWeight(Map<Object, Integer> map) {
        for (Map.Entry<Object, Integer> entry : map.entrySet()) {
            add(entry);
        }
    }

    /** Checks the value of each object in map.
     * @param map is passed to this method from the constructor.
     * @throws RandomGeneratorReturnException is thrown if value is less than 0
     * (as the probability of generation must be 0 or greater). */
    private void valueCheck(Map<Object, Integer> map) throws RandomGeneratorReturnException {
        for (Map.Entry<Object, Integer> entry : map.entrySet()) {
            if (entry.getValue() < 0) {
                throw new RandomGeneratorReturnException("Probability cannot be less than 0!");
            }
        }
    }

    /** This exception is thrown when given conditions do not allow to return an object. */
    private class RandomGeneratorReturnException extends Exception {
        RandomGeneratorReturnException(String msg) {
            super(msg);
        }
    }
}
