package com.vacariiurii;

import java.util.AbstractMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws Exception {
        LinkedHashMap<Object, Integer> objects = new LinkedHashMap<>();
        objects.put("one", 1);
        objects.put("nine", 9);
        objects.put("two", 2);
        objects.put("four", 4);
        objects.put("ten", 10);
        objects.put("five", 5);

        RandomObjectGeneratorBaseImpl generator = new RandomObjectGeneratorBaseImpl(objects);
        Map.Entry<Object, Integer> tuple = new AbstractMap.SimpleEntry<>("three", 3);
        generator.add(tuple);

        for(int i = 0; i < 100; i++) {
            System.out.println(generator.next());
        }
    }
}
